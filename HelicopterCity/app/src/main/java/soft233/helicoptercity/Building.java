package soft233.helicoptercity;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;


import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Josh on 14/04/15.
 */
public class Building {private Bitmap image;
    private int x;
    private int y=400;
    private int dx=-5;
    Random R = new Random();
    long width;
    static ArrayList<Building> buildings = new ArrayList<Building>();




    public Building(Bitmap res){

        this.image=res;
        buildings.add(this);

    }

    public void update(){

        this.x+=this.dx;
        if(this.x<-(this.width)){
            this.x=-110;
            this.y=(R.nextInt(100)+350);
            System.out.println("triggered");

        }

    }

    public void draw(Canvas canvas){

        canvas.drawBitmap(this.image,this.x,this.y,null);

        //the number below sets when the image is redrawn on the other side
        if(this.x<=0){
            this.y=(R.nextInt(100)+350);
            canvas.drawBitmap(this.image,this.x+width,this.y,null);
            this.x += this.width;
        }

    }

    public void setVector(int dx){

        this.dx=dx;
    }

    public void setX(int x){

        this.x=x;
    }

    public void setWidth(long width){

        this.width=width;
    }

    public Rect getRect(){

        return new Rect(this.x,this.y,this.x+98,this.y+128);
    }

    public int getX(){

        return x;
    }

    public int getY(){

        return y;
    }
}
