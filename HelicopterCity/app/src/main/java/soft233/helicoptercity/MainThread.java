package soft233.helicoptercity;

import android.graphics.Canvas;
import android.util.Log;
import android.view.SurfaceHolder;

/**
 * Created by Josh on 14/04/15.
 */
public class MainThread extends Thread {

    private int FPS = 30;
    private double averageFPS;
    private SurfaceHolder surfaceHolder;
    private GamePanel gamePanel;
    private boolean running;
    public static Canvas canvas;
    public int score=0;
    public int restartTime =0;

    public MainThread(SurfaceHolder surfaceHolder, GamePanel gamePanel){
        super();
        this.surfaceHolder = surfaceHolder;
        this.gamePanel = gamePanel;
        this.score = 0;
        this.restartTime=0;

    }

    @Override
    public void run(){

        long startTime;
        long timeMillis;
        long waitTime;
        long totalTime=0;
        int frameCount=0;
        long targetTime=1000/FPS;


        while(running){
            startTime = System.nanoTime();
            canvas=null;

            try {
                canvas = this.surfaceHolder.lockCanvas();

                synchronized (surfaceHolder) {
                    this.gamePanel.update();
                    this.gamePanel.draw(canvas);
                }
            }
            catch(Exception e){}
            finally {
                if(canvas!=null){
                    try {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }

            timeMillis = (System.nanoTime()-startTime)/1000000;
            waitTime = targetTime-timeMillis;

            try{
                this.sleep(waitTime);
            }
            catch (Exception e){}


            totalTime+= System.nanoTime()-startTime;
            frameCount++;
            if(frameCount == FPS){
                averageFPS = 1000/((totalTime/frameCount)/1000000);
                frameCount=0;
                totalTime=0;
                System.out.println(averageFPS);
                score++;
                restartTime++;

            }

        }

    }

    public void setRunning(boolean b){

        running=b;

    }

    public void resetScore(){

        score=0;

    }
    public int getScore(){

        return score;
    }
    public void resetRestartTime(){

        restartTime=0;

    }
    public int getRestartTime(){

        return restartTime;
    }


}
