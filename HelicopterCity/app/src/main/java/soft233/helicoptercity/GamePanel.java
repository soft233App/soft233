package soft233.helicoptercity;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;


import com.swarmconnect.Swarm;
import com.swarmconnect.SwarmLeaderboard;

import org.json.JSONObject;

import static android.support.v4.app.ActivityCompat.startActivity;

/**
 * Created by Josh on 14/04/15.
 */
public class GamePanel extends SurfaceView implements SurfaceHolder.Callback, SensorEventListener {

    public static final int WIDTH = 900, HEIGHT = 550;
    private MainThread thread;
    private Background bg;
    private Building buildingRed;
    private Building buildingBlue;
    private Building buildingGray;
    private Building buildingOrange;
    private Building buildingGreen;
    private Building buildingPurple;
    private CollisionObject object1;
    private Helicopter heli;
    private SensorManager senSensorManager;
    private Sensor senAccelerometer;
    Context context2;


    MainActivity MainActivity = null;


    public GamePanel(Context context) {

        super(context);

        context2 = context;
        getHolder().addCallback(this);

        thread = new MainThread(getHolder(), this);

        setFocusable(true);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

        //Stopping thread, can take multiple attempts
        boolean retry = true;
        while (retry) {
            try {
                thread.setRunning(false);
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            retry = false;
        }


    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        //Sensor
        senSensorManager = (SensorManager) context2.getSystemService(Context.SENSOR_SERVICE);

        senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);


        //Decided Background
        if (FetchWeather.getWeather() == null) {
            bg = new Background(BitmapFactory.decodeResource(getResources(), R.drawable.backgroundclouds));
        } else {
            bg = new Background(BitmapFactory.decodeResource(getResources(), R.drawable.backgroundsunny));
        }


        //Load asset images
        object1 = new CollisionObject(BitmapFactory.decodeResource(getResources(), R.drawable.plane),BitmapFactory.decodeResource(getResources(), R.drawable.hotair),BitmapFactory.decodeResource(getResources(), R.drawable.helicol));
        heli = new Helicopter(BitmapFactory.decodeResource(getResources(), R.drawable.helidown));
        buildingRed = new Building(BitmapFactory.decodeResource(getResources(), R.drawable.buildingred));
        buildingBlue = new Building(BitmapFactory.decodeResource(getResources(), R.drawable.buildingblue));
        buildingGray = new Building(BitmapFactory.decodeResource(getResources(), R.drawable.buildinggray));
        buildingOrange = new Building(BitmapFactory.decodeResource(getResources(), R.drawable.buildingorange));
        buildingGreen = new Building(BitmapFactory.decodeResource(getResources(), R.drawable.buildinggreen));
        buildingPurple = new Building(BitmapFactory.decodeResource(getResources(), R.drawable.buildingpurple));

        //Share widths
        buildingBlue.setWidth(getWidth());
        buildingRed.setWidth(getWidth());
        buildingGray.setWidth(getWidth());
        buildingOrange.setWidth(getWidth());
        buildingGreen.setWidth(getWidth());
        buildingPurple.setWidth(getWidth());
        object1.setWidth(getWidth());
        //Set Vector Speeds
        bg.setVector(-4);

        //Set Gaps
        placeBuildings();

        //Start Thread
        thread.setRunning(true);
        thread.start();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        //Tap play using touch screen, single tap to change direction
        if (heli.up) {
            heli.setImage(BitmapFactory.decodeResource(getResources(), R.drawable.heliup));
            heli.setVector(-5);
        } else if (!heli.up) {
            heli.setImage(BitmapFactory.decodeResource(getResources(), R.drawable.helidown));
            heli.setVector(5);
        }
        return super.onTouchEvent(event);
    }

    public void update() {

        //Update assets
        if(heli.playing) {
            bg.update();
            heli.update();
            buildingRed.update();
            buildingBlue.update();
            buildingGray.update();
            buildingOrange.update();
            buildingGreen.update();
            buildingPurple.update();
            object1.update();

            //Collision between buildings and heli
            for (Building b : Building.buildings) {
                if (Rect.intersects(heli.getRect(), b.getRect())) {
                    //Test
                    System.out.println("Intersection");
                    endOfGame();
                }
            }
            if (Rect.intersects(heli.getRect(), object1.getRect())) {
                //Test
                System.out.println("Intersection");
                endOfGame();
            }
            if(heli.getY()==0){
                System.out.println("Out of screen");
                endOfGame();
            }
            if(heli.getY()==getHeight()){
                System.out.println("Out of screen");
                endOfGame();
            }
        }else{
            if(thread.getRestartTime()==5){
                heli.setPlaying(true);
                heli.setVector(-5);
                thread.resetScore();
            }
        }

    }

    @Override
    public void draw(Canvas canvus) {

        final float scaleFactorX = getWidth() / (WIDTH);
        final float scaleFactorY = getHeight() / (HEIGHT);


        if (canvus != null) {
            final int savedState = canvus.save();
            canvus.scale(scaleFactorX, scaleFactorY);
            bg.draw(canvus);
            buildingRed.draw(canvus);
            buildingBlue.draw(canvus);
            buildingGray.draw(canvus);
            buildingOrange.draw(canvus);
            buildingGreen.draw(canvus);
            buildingPurple.draw(canvus);
            object1.draw(canvus);
            heli.draw(canvus);
            canvus.restoreToCount(savedState);
        }
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize(75);

        if (canvus != null) {
            if (heli.getPlaying()){
            canvus.drawText("Score: " + thread.getScore(),150,100,paint);}
            else{canvus.drawText("Restarting in: " + (5 - thread.getRestartTime()),280,100,paint);}
        }


    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        float[] sense = new float[3];

        sense = event.values.clone();

        double normalSense = Math.sqrt(sense[0] * sense[0] + sense[1] * sense[1] + sense[2] * sense[2]);

        sense[0] = sense[0] / (float) normalSense;
        sense[1] = sense[1] / (float) normalSense;
        sense[2] = sense[2] / (float) normalSense;

        int inc = (int) Math.round(Math.toDegrees(Math.acos(sense[2])));

        if (inc < 25 || inc > 100) {

            heli.setImage(BitmapFactory.decodeResource(getResources(), R.drawable.heliup));
            heli.setVector(-5);
            System.out.println("between 155-25");

        } else if(inc >25 || inc <100 ) {
            heli.setImage(BitmapFactory.decodeResource(getResources(), R.drawable.helidown));
            heli.setVector(5);
            System.out.println("between 25-155");
        }

    }

    public void endOfGame(){

        System.out.println("End Level");
        heli.setPlaying(false);
        SwarmLeaderboard.submitScore(19375, thread.getScore());
        heli.setY(250);
        object1.setX(getWidth());
        placeBuildings();
        thread.resetScore();
        thread.resetRestartTime();



    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy)
    {

    }

    public void placeBuildings(){
        buildingRed.setX(0);
        buildingBlue.setX(150);
        buildingGray.setX(300);
        buildingOrange.setX(450);
        buildingGreen.setX(600);
        buildingPurple.setX(750);
    }




}
