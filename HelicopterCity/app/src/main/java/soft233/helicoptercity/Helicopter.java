package soft233.helicoptercity;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

import java.util.Random;

/**
 * Created by Josh on 14/04/15.
 */
public class Helicopter {

    private Bitmap image;
    private int x=0;
    private int y=250;
    private int dy;
    public boolean up=false;
    public boolean playing = true;



    public Helicopter(Bitmap res){

        image=res;
        this.playing=true;

    }

    public void update(){

        y+=dy;

    }

    public void draw(Canvas canvas){

        canvas.drawBitmap(image,x,y,null);

    }

    public void setVector(int dy){

        this.dy=dy;
    }

    public void setImage(Bitmap image){
        if(!this.up)
        {this.up=true;}
        else if (this.up){
            this.up=false;
        }
        this.image=image;

    }


    public Rect getRect(){

        return new Rect(this.x,this.y,this.x+73,this.y+57);
    }

    public int getX(){

        return x;
    }

    public int getY(){

        return y;
    }

    public void setY(int newy){

        this.y = newy;
    }

    public void setPlaying(boolean play){
        this.playing=play;

    }


    public boolean getPlaying(){

        return this.playing;
    }

}
