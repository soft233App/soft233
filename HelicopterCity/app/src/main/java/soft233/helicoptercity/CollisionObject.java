package soft233.helicoptercity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;

import java.util.Random;

/**
 * Created by Josh on 16/04/15.
 */
public class CollisionObject {


    private Bitmap image;
    private Bitmap image1;
    private Bitmap image2;
    private Bitmap image3;
    private int x=450;
    private int y=200;
    private int dx = -5;
    private long width;
    Random R = new Random();



    public CollisionObject(Bitmap image11, Bitmap image22, Bitmap image33 ){

            image1 = image11;
            image2 = image22;
            image3 = image33;
        newImage();

    }

    public void update(){

        this.x+=this.dx;
        if(this.x<-(this.width)){
            this.x=-73;
            this.y=(R.nextInt(100)+150);
            System.out.println("triggered");

        }
    }

    public void draw(Canvas canvas){

        canvas.drawBitmap(this.image,this.x,this.y,null);

        //the number below sets when the image is redrawn on the other side
        if(this.x<=-73){
            this.y=(R.nextInt(100)+150);
            canvas.drawBitmap(this.image,this.x+width,this.y,null);
            this.x += this.width;
            newImage();
        }

    }

    public void setVector(int dx){

        this.dx=dx;
    }

    public void setWidth(long width){

        this.width=width;
    }

    public void setImage(Bitmap image){

        this.image=image;

    }

    public void setX(int newX){

        this.x=newX;

    }


    public Rect getRect(){

        return new Rect(this.x,this.y,this.x+73,this.y+57);

    }

    public int getX(){

        return x;
    }

    public int getY(){

        return y;
    }

    public void newImage(){

        int i = R.nextInt(3);
        switch (i){
            case 0:
                image = image1;
                dx = -10;
                        break;
            case 1:
                image = image2;
                dx = -2;
                break;
            case 2:
                image = image3;
                dx = -6;
                break;
        }
    }


}
