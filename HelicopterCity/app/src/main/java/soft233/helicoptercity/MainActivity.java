package soft233.helicoptercity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;


import com.swarmconnect.Swarm;
import com.swarmconnect.SwarmActivity;

import org.json.JSONObject;



public class MainActivity extends SwarmActivity implements View.OnClickListener {
    GamePanel     GamePanel          = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Swarm.init(this, 17169, "be7b034391ace65ecee9fca914e65214");
        setContentView(R.layout.activity_main);

        Button buttonLaunch = (Button) findViewById(R.id.btn_play);
        buttonLaunch.setOnClickListener((View.OnClickListener) this);

        Button buttonLeader = (Button) findViewById(R.id.btn_leader);
        buttonLeader.setOnClickListener((View.OnClickListener) this);

        //setContentView(new GamePanel(this));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause(){
        super.onPause();
        finish();
    }



    @Override
    public void onClick(View v)
    {
        //Belt and braces check; there is only one button!//

        if (v.getId() == R.id.btn_play)
        {
            if (GamePanel == null) GamePanel = new GamePanel(this);
            //GamePanel.surfaceCreated();

            setContentView(GamePanel);

        }
        if (v.getId() == R.id.btn_leader)
        {

            Swarm.showLeaderboards();

        }

    }


}
